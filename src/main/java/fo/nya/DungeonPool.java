package fo.nya;

import java.util.*;

/**
 * Created by 0da on 04.09.2023 17:45; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 * <br>
 * Класс для комбинации комнат подземелья в цепочку из неопределенного количества комнат.
 */
public class DungeonPool {

    /**
     * Уникальный перечень комнат из которых будет сгенерирована последовательность.
     */
    private final Dungeon[] dungeons;

    /**
     * Конструктор, создает новый генератор подземелий на основе переданных 'комнат'.
     *
     * @param dungeons комнаты подземелий которые будут использоваться в генерации последовательностей, будут использованы только уникальные комнаты.
     * @throws IllegalArgumentException в случае если уникальных подземелий меньше двух.
     * @throws IllegalStateException    в случае если одно из подземелий не имеет входа, выхода, или не проходимо.
     */
    public DungeonPool(Collection<Dungeon> dungeons) {

        this.dungeons = dungeons.stream().distinct().toArray(Dungeon[]::new);

        if (this.dungeons.length < 2) {
            throw new IllegalArgumentException("Too few unique dungeons, should have at least 2.");
        }

        final int height = this.dungeons[0].height();

        // Проверка того что все высоты одинаковы:
        for (int i = 1; i < this.dungeons.length; i++) {
            int h = this.dungeons[i].height();

            if (h != height) {
                throw new IllegalArgumentException("The height of the dungeons is not the same, some of them are "
                                                   + Math.min(height, h) + ", some are " + Math.max(height, h) + ".");
            }
        }

        // Проверка того что все проходимы:
        for (Dungeon dungeon : this.dungeons) {

            int in = -1;
            int out = -1;

            for (int y = 0; y < dungeon.height(); y++) {
                if (in < 0 && dungeon.get(0, y) == Block.AIR) in = y;
                if (out < 0 && dungeon.get(dungeon.width() - 1, y) == Block.AIR) out = y;
            }

            if (in < 0) {
                throw new IllegalStateException("The dungeon has no entrance:  \n" + dungeon);
            }

            if (out < 0) {
                throw new IllegalStateException("The dungeon has no exits:  \n" + dungeon);
            }

            // Обход в ширину для проверки того, что вход соединен с выходом:

            // @Value class Point{int x, y;} | record Point(int x, int y) {}
            class Point {
                final int x, y;

                public Point(int x, int y) {
                    this.x = x;
                    this.y = y;
                }

                @Override public boolean equals(Object o) {
                    if (this == o) return true;
                    if (!(o instanceof Point)) return false;
                    Point point = (Point) o;
                    return x == point.x && y == point.y;
                }

                @Override public int hashCode() {
                    return Objects.hash(x, y);
                }
            }

            Queue<Point> points = new LinkedList<>();
            Set<Point> checked = new HashSet<>();
            points.add(new Point(0, in));

            while (true) {
                Point poll = points.poll();
                if (poll == null) {
                    throw new IllegalStateException("Dungeon is not passable: \n" + dungeon);
                }

                if (checked.contains(poll)) continue;
                checked.add(poll);

                if (dungeon.get(poll.x, poll.y) == Block.GROUND) continue;

                if (poll.y == out && poll.x == dungeon.width() - 1) break;

                if (poll.x > 0) points.add(new Point(poll.x - 1, poll.y));
                if (poll.y > 0) points.add(new Point(poll.x, poll.y - 1));
                if (poll.x < dungeon.width() - 1) points.add(new Point(poll.x + 1, poll.y));
                if (poll.y < dungeon.height() - 1) points.add(new Point(poll.x, poll.y + 1));

            }
        }
    }

    /**
     * Метод генерирует новую последовательность подземелий, гарантируется что последовательность не будет содержать дважды подряд одну комнату.
     *
     * @param random генератор случайных чисел для использования алгоритмом.
     * @param length требуемая длинна последовательности.
     * @return новый список и последовательностью из {@code length} комнат.
     * @throws IllegalArgumentException в случае если аргумент {@code length} меньше одного.
     * @throws IllegalStateException    в случае если для какой-то комнаты не нашлось другой сопоставимой.
     */
    public ArrayList<Dungeon> createXSequence(Random random, int length) {

        if (length < 1) {
            throw new IllegalArgumentException("Sequence length cannot be less than 1.");
        }

        ArrayList<Dungeon> result = new ArrayList<>(length);

        Dungeon previous = null;

        outer:
        for (int r = 0; r < length; r++) {
            int offset = random.nextInt(dungeons.length);

            for (int d = 0; d < dungeons.length; d++) {
                int index = (offset + d) % dungeons.length;
                Dungeon current = dungeons[index];

                if (current == previous) continue;

                for (int y = 0; y < current.height(); y++) {

                    if (previous == null
                        || previous.get(previous.width() - 1, y) == Block.AIR && current.get(0, y) == Block.AIR) {
                        previous = current;
                        result.add(current);
                        continue outer;
                    }
                }
            }

            throw new IllegalStateException("Failed to create a dungeon sequence because none of the dungeons fit this: \n"
                                            + previous);

        }

        return result;
    }
}

package fo.nya;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by 0da on 04.09.2023 18:27; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 */
class DungeonPoolTest {
    @Test void notEnoughUniqDungeons() {

        Dungeon a = new Dungeon(1, 1);
        Dungeon b = new Dungeon(2, 1);

        Assertions.assertThatThrownBy(() -> new DungeonPool(Collections.singletonList(a)))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Too few unique dungeons, should have at least 2.");


        Assertions.assertThatThrownBy(() -> new DungeonPool(Arrays.asList(a, a)))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Too few unique dungeons, should have at least 2.");


        Assertions.assertThatCode(() -> new DungeonPool(Arrays.asList(a, b)))
                .doesNotThrowAnyException();
    }

    @Test void notSameHeight() {
        Dungeon a = new Dungeon(1, 13);
        Dungeon b = new Dungeon(2, 13);
        Dungeon c = new Dungeon(3, 17);

        Assertions.assertThatCode(() -> new DungeonPool(Arrays.asList(a, b)))
                .doesNotThrowAnyException();

        Assertions.assertThatThrownBy(() -> new DungeonPool(Arrays.asList(a, c)))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("The height of the dungeons is not the same, some of them are 13, some are 17.");
    }

    @Test void noEntranceOrExit() {
        Dungeon a = new Dungeon(4, 1);
        Dungeon b = new Dungeon(5, 1);
        Dungeon c = new Dungeon(6, 1);

        a.set(0, 0, Block.GROUND);
        b.set(4, 0, Block.GROUND);


        Assertions.assertThatThrownBy(() -> new DungeonPool(Arrays.asList(a, c)))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageStartingWith("The dungeon has no entrance");


        Assertions.assertThatThrownBy(() -> new DungeonPool(Arrays.asList(b, c)))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageStartingWith("The dungeon has no exits");
    }

    @Test void noWay() {
        Dungeon a = new Dungeon(4, 2);
        Dungeon b = new Dungeon(5, 2);
        Dungeon c = new Dungeon(6, 2);

        a.set(1, 0, Block.GROUND);
        a.set(2, 1, Block.GROUND);

        b.set(3, 0, Block.GROUND);
        b.set(2, 1, Block.GROUND);

        Assertions.assertThatThrownBy(() -> new DungeonPool(Arrays.asList(a, c)))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageStartingWith("Dungeon is not passable");


        Assertions.assertThatThrownBy(() -> new DungeonPool(Arrays.asList(b, c)))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageStartingWith("Dungeon is not passable");

    }

    @Test void simpleSequence() {
        Dungeon a = new Dungeon(3, 2);
        Dungeon b = new Dungeon(5, 2);

        DungeonPool pool = new DungeonPool(Arrays.asList(a, b));

        ArrayList<Dungeon> sequence = pool.createXSequence(ThreadLocalRandom.current(), 97);

        Assertions.assertThat(sequence)
                .hasSize(97);

        for (int i = 1; i < sequence.size(); i++) {
            Assertions.assertThat(sequence.get(i - 1))
                    .isNotEqualTo(sequence.get(i));
        }
    }

    @Test void nonMatchable() {
        Dungeon a = new Dungeon(1, 2);
        Dungeon b = new Dungeon(1, 2);

        a.set(0, 0, Block.GROUND);
        b.set(0, 1, Block.GROUND);

        DungeonPool pool = new DungeonPool(Arrays.asList(a, b));

        Assertions.assertThatThrownBy(() -> pool.createXSequence(ThreadLocalRandom.current(), 7))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageStartingWith("Failed to create a dungeon sequence because none of the dungeons fit");
    }

    @Test void shortSequence() {
        Dungeon a = new Dungeon(1, 2);
        Dungeon b = new Dungeon(2, 2);

        DungeonPool pool = new DungeonPool(Arrays.asList(a, b));

        Assertions.assertThatCode(() -> pool.createXSequence(ThreadLocalRandom.current(), 2))
                .doesNotThrowAnyException();

        Assertions.assertThatCode(() -> pool.createXSequence(ThreadLocalRandom.current(), 1))
                .doesNotThrowAnyException();

        Assertions.assertThatThrownBy(() -> pool.createXSequence(ThreadLocalRandom.current(), 0))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Sequence length cannot be less than 1.");

        Assertions.assertThatThrownBy(() -> pool.createXSequence(ThreadLocalRandom.current(), -1))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Sequence length cannot be less than 1.");
    }

    @Test void connectivityTest() {

        // # . #
        // # . .
        // . . #

        Dungeon a = new Dungeon(3, 3);
        a.set(0, 0, Block.GROUND);
        a.set(0, 1, Block.GROUND);

        a.set(2, 0, Block.GROUND);
        a.set(2, 2, Block.GROUND);

        // # . .
        // . . #
        // # . #
        Dungeon b = new Dungeon(3, 3);

        b.set(0, 0, Block.GROUND);
        b.set(0, 2, Block.GROUND);

        b.set(2, 1, Block.GROUND);
        b.set(2, 2, Block.GROUND);

        // . . #
        // # . #
        // # . .
        Dungeon c = new Dungeon(3, 3);

        c.set(0, 1, Block.GROUND);
        c.set(0, 2, Block.GROUND);

        c.set(2, 0, Block.GROUND);
        c.set(2, 1, Block.GROUND);

        // System.out.println((a + "\n\n" + b + "\n\n" + c).replace('G', '#').replace('A', '.'));

        DungeonPool pool = new DungeonPool(Arrays.asList(a, b, c));

        ArrayList<Dungeon> sequence = pool.createXSequence(ThreadLocalRandom.current(), 999);

        for (int i = 1; i < sequence.size(); i++) {
            Dungeon current = sequence.get(i);
            Dungeon previous = sequence.get(i - 1);

            if (current == a) {
                Assertions.assertThat(previous).isSameAs(c);
            } else if (current == b) {
                Assertions.assertThat(previous).isSameAs(a);
            } else if (current == c) {
                Assertions.assertThat(previous).isSameAs(b);
            } else {
                Assertions.fail("Unknown room.");
            }
        }

    }
}